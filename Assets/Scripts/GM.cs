using UnityEngine;
using System.Collections;

public class GM : MonoBehaviour {

	public static GameObject player, foodCell, foodCellsParent;
	public static Player thisPlayer;

	void Awake(){
		Application.targetFrameRate = 60;
	}

	void Start () {
		//player = Instantiate(Resources.Load("Player")) as GameObject;
		foodCell = Resources.Load("food") as GameObject;
		foodCellsParent = GameObject.Find("FoodCells");
		//thisPlayer = player.GetComponent<Player>();
	}

	public static void AddElixirs(int count) {
		int countInt = PlayerPrefs.GetInt("Elixirs");
		countInt += count;
		PlayerPrefs.SetInt("Elixirs", countInt);
	}
}
