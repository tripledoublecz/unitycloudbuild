using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Cell : Dot {

	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;
	Vector3 _scale = Vector3.zero;
	int _mass = 0;
	
	Quaternion syncStartRotation = Quaternion.identity;
	Quaternion syncEndRotation = Quaternion.identity;
	
	public Rigidbody2D rigidBody;
	
	public Transform myParent;
	
	public Player myPlayer;
	
	public int mass = 2;
	bool followLeader = true;
	float endTime, uniteTime;
	bool followAfterShot = false;
	bool connected = false;
	public bool splited = false,
				virusSplited = false;
	float splitTimer = 0.6f;
	float splitSpeed;
	float afterSplitTimer;

	GameObject arrow, insideCell, spriteCell, splitBreak, nicknameObj;
	DistanceJoint2D joint;
	float x;
	public bool eat;
	public GameObject eatedBy;
	bool control = false;
	Text nicknameText;

	void Awake() {
		lastSynchronizationTime = Time.time;
	}
	
	void Start () {
		afterSplitTimer = 8+mass/4;
		rigidBody = gameObject.GetComponent<Rigidbody2D>();

		foreach (Transform child in transform) {
			if (child.tag == "Arrow") {
				arrow = child.gameObject;
			}
			if (child.tag == "Inside_cell") {
				insideCell = child.gameObject;
			}
			if (child.name == "Cell_sprite") {
				spriteCell = child.gameObject;
			}
			if (child.name == "SplitBreak") {
				splitBreak = child.gameObject;
			}
			if (child.name == "Canvas") {
				nicknameText = child.GetComponentInChildren<Text>();
            }
		}

		splitBreak.SetActive(false);
		if (GetComponent<PhotonView>().isMine)
			splitBreak.SetActive(true);

		myPlayer = transform.parent.GetComponent<Player>();

		if (GetComponent<PhotonView>().isMine){
            if (myPlayer.leaderCell == this) {
				if (PlayerPrefs.GetInt("Premium") == 0) {
					Debug.Log("Non Premium");
					ChangeColor();
				} else {
					//PhotonNetwork.Destroy(insideCell);
					//ChangeSkinTo(PlayerPrefs.GetString("SkinSet"));
					GetComponent<SpriteRenderer>().sprite = null;
				}
			} else {
				if (PlayerPrefs.GetInt("Premium") == 0) {
					Color leaderColor = myPlayer.leaderCellObj.GetComponent<SpriteRenderer>().color;
					ChangeColorTo(new Vector3(leaderColor.r, leaderColor.g, leaderColor.b));
				} else {
					//PhotonNetwork.Destroy(insideCell);
					//ChangeSkinTo(PlayerPrefs.GetString("SkinSet"));
					GetComponent<SpriteRenderer>().sprite = null;
				}
				endTime = 0;
			}

			GetComponent<PhotonView>().RPC("SetNickname", PhotonTargets.AllBuffered, PlayerPrefs.GetString("Nickname"));

			//ChangeSkinTo(PlayerPrefs.GetString("SkinSet"));
			//GetComponent<PhotonView>().RPC("ChangeSkinTo", PhotonTargets.AllBuffered, PlayerPrefs.GetString("SkinSet"));
		}

		//if (GetComponent<PhotonView>().isMine)
		//	GetComponent<PhotonView>().RPC("ChangeSkinTo", PhotonTargets.AllBuffered, PlayerPrefs.GetString("SkinSet"));
        
        
        maxSpeed = 5 - mass * 0.01f * splitSpeed;
        splitSpeed = maxSpeed * 4;
        
        Color thisColor = GetComponent<SpriteRenderer>().color;
		if (insideCell)
		insideCell.GetComponent<SpriteRenderer>().color = new Color(thisColor.r+0.3f, thisColor.g+0.3f, thisColor.b+0.3f, 1f);
		//insideCell.GetComponent<SpriteRenderer>().color = Color.green;
		splitTimer = 0.4f;
    }

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		Vector3 syncPosition = Vector3.zero;
		Vector3 syncVelocity = Vector3.zero;
		_scale = Vector3.zero;
		
		if (stream.isWriting) {
			stream.SendNext(transform.position);
			stream.SendNext(transform.localScale);
			stream.SendNext(mass);
			stream.SendNext(transform.localRotation);
        } else {
			syncEndPosition = (Vector3)stream.ReceiveNext();
			syncStartPosition = transform.position;
			_scale = (Vector3)stream.ReceiveNext();
			_mass = (int)stream.ReceiveNext();
			syncEndRotation = (Quaternion)stream.ReceiveNext();
			syncStartRotation = transform.localRotation;
			
			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;
        }
    }
    
    void Update () {

		if (myPlayer.leaderCell != this) {
				if (afterSplitTimer <= 0) {
					if (GetComponent<PhotonView>().isMine)
						splitBreak.SetActive(false);
				} else {
					afterSplitTimer -= Time.deltaTime;
				}
		} else {
			splitBreak.SetActive(true);
		}

		if (GetComponent<PhotonView>().isMine) {
			if (myPlayer.leaderCell != this) {
				rigidBody.mass = 1;
				arrow.SetActive(false);
			} else {
				rigidBody.mass = 1000;
				arrow.SetActive(true);
			}

			//scale = 0.1f * Mathf.Log(mass,2f);
			// S = pi*r^2

			//scale = (Mathf.Sqrt(Mathf.Sqrt(mass))/Mathf.PI)/2;
			scale = 0.04f + ((2*Mathf.Sqrt(mass/3.14f))/16);

			maxSpeed = 4 - scale/4;
			transform.position = new Vector3(transform.position.x, transform.position.y, -2-scale + 0.04f);
		}
	}
	
	void FixedUpdate () {
        if (GetComponent<PhotonView>().isMine && !eat) {
			this.gameObject.transform.localScale = Vector3.Lerp (transform.localScale, new Vector3(scale,scale,scale), 10f * Time.deltaTime);

			if (splited) {
				SplitMove();
			} else if (virusSplited) {
				VirusSplitMove();
			} else {
				if (transform.parent.childCount > 1 && myPlayer.leaderCell != this) {
					//Pohyb dotky + pomalé přibližování
					Vector2 uniteMove = Vector2.Lerp(rigidBody.position + myPlayer.move * maxSpeed * Time.deltaTime, myPlayer.leaderCellObj.transform.position, 0.2f * Time.deltaTime);
					rigidBody.MovePosition(uniteMove);
				} else {
					//Pohyb dotky
					rigidBody.MovePosition(rigidBody.position + myPlayer.move * maxSpeed * Time.deltaTime);
				}
			}

			//Rotace dotky
			if (myPlayer.leaderCell == this && myPlayer.move.x+myPlayer.move.y != 0) {
				Vector3 vectorToTarget = new Vector3(myPlayer.move.x*1000, myPlayer.move.y*1000, 5) - transform.position;
				float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
				Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
				transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 4000);
			} else {
				transform.rotation = myPlayer.leaderCellObj.transform.rotation;
			}
		} else {
			SyncedMovement();
		}
	}

	private void SyncedMovement() {
		syncTime += Time.deltaTime;
		
		rigidBody.position = Vector3.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);
		transform.localScale = _scale;
		mass = _mass;
		transform.localRotation = Quaternion.Lerp(syncStartRotation, syncEndRotation, syncTime / syncDelay);
	}

	void FollowLeader() {
		if (!connected) {
		//if ((Vector3.Distance(transform.position,myPlayer.leaderCellObj.transform.position)) > myPlayer.leaderCell.transform.localScale.x*7.456) {
			Vector2 dir = myPlayer.leaderCellObj.transform.position - transform.position;
			//dir = (myPlayer.leaderCellObj.transform.position - transform.position).normalized * 2;
			rigidBody.velocity = Vector2.Lerp(transform.position, dir, Time.deltaTime * 320);
		//}
		}
	}

	void OnTriggerStay2D(Collider2D otherCore){
		if (otherCore.tag == "Food") {
			if (otherCore.GetComponent<Food>()) {
				otherCore.GetComponent<Food>().eatedBy = gameObject;
				otherCore.GetComponent<Food>().eat = true;
				if (Vector2.Distance(otherCore.transform.position, transform.position) < 0.5f) {
					GetComponent<PhotonView>().RPC("DestroyThis", PhotonTargets.AllBuffered, otherCore.GetComponent<PhotonView>().viewID);
				}
			}
		}
		if (otherCore.tag == "Virus" && mass > 60) {
			if (otherCore.GetComponent<PhotonView>().isMine) {
				//PhotonNetwork.Instantiate("Virus_dead_effect", otherCore.transform.position, Quaternion.identity, 0);
				//PhotonNetwork.RemoveRPCs(otherCore.gameObject.GetComponent<PhotonView>());
				//PhotonNetwork.Destroy(otherCore.gameObject);
			}
			PhotonNetwork.Instantiate("Virus_dead_effect", otherCore.transform.position, Quaternion.identity, 0);
			GetComponent<PhotonView>().RPC("DestroyThis", PhotonTargets.AllBuffered, otherCore.GetComponent<PhotonView>().viewID);
			VirusSplit ();
		}
    }
    
    void OnTriggerEnter2D(Collider2D otherCore) {
		if (otherCore.tag == "BotCore" && mass-mass*0.1f > otherCore.transform.parent.GetComponent<PlayerBot>().mass) {
			mass += otherCore.transform.parent.GetComponent<PlayerBot>().mass;
			Destroy(otherCore.transform.parent.gameObject);
        }
		if (otherCore.tag == "Core" && (mass > otherCore.transform.parent.GetComponent<Cell>().mass + otherCore.transform.parent.GetComponent<Cell>().mass*0.1f 
		                                || (otherCore.transform.parent.transform.parent == transform.parent && otherCore.transform.parent.GetComponent<Cell>().splited == false && myPlayer.leaderCell == this))) {
			GetComponent<PhotonView>().RPC("DestroyThis", PhotonTargets.AllBuffered, otherCore.transform.parent.gameObject.GetComponent<PhotonView>().viewID);
			mass += otherCore.transform.parent.GetComponent<Cell>().mass;
			PhotonNetwork.Instantiate("DotDeadEffect", otherCore.transform.position, Quaternion.identity, 0);
		}
    }
    
    public void Split() {
		if (mass > 20) {
			GameObject split = PhotonNetwork.Instantiate("Cell", arrow.transform.position, Quaternion.identity, 0) as GameObject;

			split.transform.parent = myPlayer.transform;
			myPlayer.GetComponent<PhotonView>().RPC("addToParent", PhotonTargets.OthersBuffered, split.GetComponent<PhotonView>().viewID);

			split.GetComponent<Cell>().mass = mass/2;
			mass -= split.GetComponent<Cell>().mass;
			split.GetComponent<Cell>().splited = true;
		}
	}

	public void SplitMove() {
		transform.Translate(Vector3.right * splitSpeed * Time.deltaTime); 
		splitTimer -= Time.deltaTime;
		
		//splitSpeed = 10;
		if (splitTimer <=0) {
			splitSpeed = Mathf.Lerp(splitSpeed, 0, 8 * Time.deltaTime);
		}
		
		if (splitSpeed <= 0.4f) {
			splited = false;
		}
	}
	
	public void VirusSplitMove() {
		splitTimer -= Time.deltaTime;
		
		//splitSpeed = 10;
		if (splitTimer <=0) {
			splitSpeed = Mathf.Lerp(splitSpeed, 0, 4 * Time.deltaTime);
		}
		
		//if (splitSpeed <= 0.4f) {
			virusSplited = false;
		//}
	}
	public void VirusSplit() {
		int r = Random.Range(4,10);
		for (int i = 1; i< r; i++) {
			Vector2 randomRadiusPos = RandomCircle(transform.position, scale + 1);
			GameObject split = PhotonNetwork.Instantiate("Cell", randomRadiusPos, Quaternion.identity, 0) as GameObject;
			split.transform.parent = myPlayer.transform;
			myPlayer.GetComponent<PhotonView>().RPC("addToParent", PhotonTargets.OthersBuffered, split.GetComponent<PhotonView>().viewID);
			split.GetComponent<Cell>().mass = mass/r;
			mass -= split.GetComponent<Cell>().mass;
			split.GetComponent<Cell>().virusSplited = true;
		}
	}
    
    void ChangeColor() {
		int randomInt = Random.Range(0,4);
		Color newColor = Color.green;
		switch(randomInt) {
			case 0: newColor = new Color(0.7f,0,0); break;
			case 1: newColor = new Color(0.7f,0,0); break;
			case 2: newColor = new Color(0,0.7f,0); break;
			case 3: newColor = new Color(0,0,0.7f); break;
			case 4: newColor = new Color(0.7f,0,0); break;
		}
		ChangeColorTo(new Vector3(newColor.r, newColor.g, newColor.b));
		//ChangeColorTo(new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)));
	}

	Vector2 RandomCircle(Vector2 center, float radius) { 
		// create random angle between 0 to 360 degrees 
		float ang = Random.value * 360; 
		Vector2 pos; 
		pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad); 
		pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad); 
		return pos; 
	}
	
	[RPC] void ChangeColorTo(Vector3 color) {
		GetComponent<SpriteRenderer>().color = new Color(color.x, color.y, color.z, 1f);
		
		if (GetComponent<PhotonView>().isMine)
			GetComponent<PhotonView>().RPC("ChangeColorTo", PhotonTargets.OthersBuffered, color);
	}

	[RPC] void SetNickname(string nick) {
		nicknameText.text = nick;
		
		if (GetComponent<PhotonView>().isMine)
			GetComponent<PhotonView>().RPC("SetNickname", PhotonTargets.OthersBuffered, nick);
    }

	[RPC] void ChangeSkinTo(string skinName) {
		GetComponent<SpriteRenderer>().color = Color.white;
		GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Avatars/" + skinName);

		if (GetComponent<PhotonView>().isMine)	
			GetComponent<PhotonView>().RPC("ChangeSkinTo", PhotonTargets.OthersBuffered, skinName);
	}

	[RPC] void DestroyThis(int id) {
		if (PhotonView.Find(id).gameObject.GetComponent<PhotonView>().isMine)
			PhotonNetwork.Destroy(PhotonView.Find(id).gameObject);
		Destroy (PhotonView.Find(id).gameObject);
	}
}
