﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Soomla;
using Soomla.Store;
using Soomla.Store.DotWars;

namespace Soomla.Store.DotWars {
	public class Skin_store : MonoBehaviour {

		GameObject storeCollection;
		public GameObject buyThisPanel;

		void Start() {
			if (!SoomlaStore.Initialized)
				SoomlaStore.Initialize(new Store());
			storeCollection = GameObject.Find("Skin_store_collection");

			LoadSkins();
		}

		public void LoadSkins() {
			Debug.Log("Load");
			foreach (Transform child in storeCollection.transform) {
				Destroy(child.gameObject);
			}
			foreach(VirtualGood vg in StoreInfo.Goods){
				if (vg.Name != "No Ads") {
					GameObject skinObject = Instantiate(Resources.Load("Skin_store")) as GameObject;
					skinObject.transform.SetParent(storeCollection.transform, false);
                    string captured = vg.ItemId;
					string capturedFullName = vg.Name;
					int capturedInt = ((PurchaseWithVirtualItem)vg.PurchaseType).Amount;
					if (StoreInventory.GetItemBalance(vg.ItemId) > 0) {
						skinObject.GetComponent<Button>().interactable = false;
					} else {
						skinObject.GetComponent<Button>().onClick.AddListener(() => { OpenBuyPanel(captured, capturedInt, capturedFullName);});
					}
					foreach (Transform child in skinObject.transform) {
						if (child.name == "Skin_image") {
							child.GetComponent<Image>().sprite = Resources.Load<Sprite>("Avatars/" + vg.Name);
						}
						if (child.name == "Button_text") {
							if (StoreInventory.GetItemBalance(vg.ItemId) > 0) {
								child.GetComponentInChildren<Text>().text = "Purchased";
							} else {
								child.GetComponentInChildren<Text>().text = "" + ((PurchaseWithVirtualItem)vg.PurchaseType).Amount + " Golds";
							}
						}
					}
				}
			}
		}

		public void OpenBuyPanel(string name, int price, string fullName) {
			buyThisPanel.SetActive(true);
			GameObject.Find("Buy_button").GetComponent<Button>().onClick.AddListener(() => { BuyThis(name, fullName);});
			GameObject.Find("BuyThis_text").GetComponent<Text>().text = "Buy for " + price + " Golds?";
		}

		public void BuyNoAds() {
			OpenBuyPanel("no_ads_item_id", 100, "No Ads");
        }
        
        public void BuyThis(string name, string fullName) {
			StoreInventory.BuyItem(name);
			buyThisPanel.SetActive(false);
			if (fullName != "No Ads")
				PlayerPrefs.SetString("SkinSet", fullName);
			LoadSkins();
		}

		public void CloseBuyThisPanel() {
			buyThisPanel.SetActive(false);
		}

	}
}
