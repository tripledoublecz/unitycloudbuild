using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SinglePlayer : MonoBehaviour {

	public GameObject playerPrefab, playMenu;
	public static GameObject foodCell, foodCellsParent;
	public static Player thisPlayer;
	int maxFoodCount;
	Button playButton;
	
	public GameObject splitGameButton;
	
	public string nickname;
	
	public int mapHeight = 32, 
	mapWidth = 32;

	void Start () {
		splitGameButton.SetActive(false);
		foodCell = Resources.Load("food") as GameObject;
		foodCellsParent = GameObject.Find("FoodCells");
		maxFoodCount = 32;

		SpawnPlayer();
		splitGameButton.SetActive(true);
	}
	
	void Update() {
		
		if (foodCellsParent.transform.childCount < maxFoodCount) {
			SpawnFood();
		}
	}

	void SpawnFood() {
		Vector3 position = new Vector3(Random.Range(-mapWidth/2f,mapWidth/2f),Random.Range(-mapHeight/2f,mapHeight/2f), 1);
		GameObject Obj = Instantiate(foodCell, position, Quaternion.identity) as GameObject;
		Obj.transform.parent = foodCellsParent.transform;
	}
	public void SpawnPlayer() {
		GameObject Obj = Instantiate(playerPrefab, Vector3.up * 5, Quaternion.identity) as GameObject;
		Obj.GetComponent<Player>().name = nickname;
	}
}
