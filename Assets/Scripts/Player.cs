using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Soomla;
using Soomla.Store;
using System.Collections;

public class Player : Entity {
	
	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;

	private Vector2 dragOrigin;
	public Vector2 pos;
	private Rigidbody2D rigidBody;
	public Vector2 move;
	public GameObject joyStickOrigin,joyStickPos;
	public GameObject cell;

	public GameObject leaderCellObj;
	public Cell leaderCell;
	int leaderMass = 0;

	public bool isGameOver = false;
	PhotonView photonView;
	bool canTouch = true;
	public Text scoreText;
	Server thisServer;
	GameObject foodCellsParent;

	float kickTimer;

	int deathCount = 0;
    
	void Awake()
	{
		cell = Resources.Load("Cell") as GameObject;
		lastSynchronizationTime = Time.time;
		if (GetComponent<PhotonView>().isMine) {
			GetComponent<PhotonView>().RPC("SetNickname", PhotonTargets.AllBuffered, name = PlayerPrefs.GetString("Nickname"));
			kickTimer = 30;
		}
	}
    
    void Start () {
		if (PlayerPrefs.GetString("SkinSet") == "") {
			PlayerPrefs.SetInt("Premium", 0);
		} else {
			PlayerPrefs.SetInt("Premium", 1);
        }
		photonView = GetComponent<PhotonView>();
        thisServer = GameObject.Find("GM").GetComponent<Server>();
		foodCellsParent = GameObject.Find("FoodCells");

		if (photonView.isMine)
		{
			CameraClass.thisPlayer = this;
			GM.thisPlayer = this;

			score = 0;

			if (StoreInventory.GetItemBalance(StoreInfo.Currencies[0].ItemId) > 0) {
				StoreInventory.TakeItem(StoreInfo.Currencies[0].ItemId, 1);
				SpawnCell();
			} else {
				GameObject.Find("GM").GetComponent<UIControl>().OpenMainMenu();
			}
			//photonView.RPC("addToParent", PhotonTargets.OthersBuffered);

			if (PlayerPrefs.GetInt("Joystick") == 1) {
				joyStickOrigin = GameObject.Find("JoyStickOrigin");
				joyStickPos = GameObject.Find("JoyStickPos");
			}

			rigidBody = gameObject.GetComponent<Rigidbody2D>();
			scoreText = GameObject.Find("Score_text").GetComponent<Text>();

		}

		Debug.Log (PhotonNetwork.room.name);
	}

	void Update () {
		score = 0;
		foreach (Transform child in transform) {
			if (child.tag == "Cell") {
				score += child.GetComponent<Cell>().mass;
			}
		}

		if (photonView.isMine)
		{
			if (score > PlayerPrefs.GetInt("HighScore")) {
				PlayerPrefs.SetInt("HighScore", score);
			}

			if (score == 0) {
				kickTimer -= Time.deltaTime;
			} else {
				kickTimer = 30;
			}

			if  (kickTimer <= 0) {
				PhotonNetwork.Destroy(gameObject);
				PhotonNetwork.Disconnect();
				GameObject.Find("GM").GetComponent<UIControl>().OpenMainMenu();
			}
			if (score == 0 && !isGameOver) {
				GameOver();
				isGameOver = true;
				/*
				if (StoreInventory.GetItemBalance(StoreInfo.Currencies[0].ItemId) > 0) {
					StoreInventory.TakeItem(StoreInfo.Currencies[0].ItemId, 1);
				}
				*/
            }

			if (!isGameOver) {
	            Joystick();
				if (Input.touchCount > 0 && PlayerPrefs.GetInt("Joystick") == 1) {
					joyStickOrigin.transform.position = dragOrigin;
					joyStickPos.transform.position = Input.GetTouch(0).position;
				}

				scoreText.text = "Score: " + CameraClass.thisPlayer.score;

				if (Input.GetKeyDown(KeyCode.E)){
					SplitCells();
				}
				if (!leaderCellObj) {
					FindLeader();
				}
			}
			if (score != 0) {
				PlayerPrefs.SetInt("LastScore", score);
			}
			if ((foodCellsParent.transform.childCount < thisServer.maxFoodCount) && PhotonNetwork.isMasterClient) {
				SpawnFood();
			}
		}
		if (Input.GetKeyDown(KeyCode.Escape)) 
			Application.Quit();
	}

	void Joystick () {

			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && canTouch) {
				dragOrigin = Input.GetTouch(0).position;
				if (PlayerPrefs.GetInt("Joystick") == 1)
					joyStickOrigin.SetActive(true);
				canTouch = false;
			}
			
			if (Input.touchCount == 0) {
				if (PlayerPrefs.GetInt("Joystick") == 1)
					joyStickOrigin.SetActive(false);
				canTouch = true;
				move = Vector2.zero;
				return;
			}
			
			if (Input.touchCount > 0)
	   			pos = Camera.main.ScreenToViewportPoint(Input.GetTouch(0).position - dragOrigin);

			Vector2 mov = new Vector2 (pos.x*10, pos.y*10);
			move = Vector2.ClampMagnitude(mov, 1);
	}
    
    void OnTriggerStay2D(Collider2D otherCore){
        if (otherCore.tag == "Core") {
			Destroy(otherCore.gameObject.transform.parent.gameObject);
			score++;
		}

		if (otherCore.tag == "Player") {
			if (otherCore.GetComponent<Player>().score < score) {
				score += otherCore.GetComponent<Player>().score;
			}
		}
	}
    
	void FindLeader() {
		leaderMass = 0;
		foreach (Transform child in transform) {
			if (child.tag == "Cell") {
				if (child.GetComponent<Cell>().mass > leaderMass) {
					leaderMass = child.GetComponent<Cell>().mass;
					leaderCellObj = child.gameObject;
					leaderCell = leaderCellObj.GetComponent<Cell>();
				}
			}
		}
	}

	public void SplitCells() {
		if (transform.childCount < 2) {
			foreach (Transform child in transform) {
				if (child.tag == "Cell") {
					child.GetComponent<Cell>().Split();
				}
			}
		}
	}

	void GameOver() {
		if (StoreInventory.GetItemBalance(StoreInfo.Currencies[0].ItemId)>0) {
			GameObject.Find("UI").GetComponent<UIControl>().gameOver.SetActive(true);
			GameObject.Find("UI").GetComponent<UIControl>().gameOver.GetComponent<GameOver>().enabled = true;
		} else {
			//ShowCountdown();
			GameObject.Find("UI").GetComponent<UIControl>().OpenMainMenu();
		}
	}

	void ShowCountdown() {
		GameObject.Find("UI").GetComponent<UIControl>().countDown.SetActive(true);
		GameObject.Find("UI").GetComponent<UIControl>().countDown.GetComponent<Countdown>().enabled = true;
	}

	void SpawnFood() {
		Vector3 position = new Vector3(Random.Range(-thisServer.mapWidth/2f,thisServer.mapWidth/2f),Random.Range(-thisServer.mapHeight/2f,thisServer.mapHeight/2f), 4);
		GameObject Obj = PhotonNetwork.Instantiate("Food", position, Quaternion.identity, 0) as GameObject;
		Obj.transform.parent = foodCellsParent.transform;
	}

	[RPC] public void addToParent(int id) {
		Transform t = PhotonView.Find(id).transform;
		t.parent = transform;
    }

	public void SpawnCell() {
		if (photonView.isMine) {
			Vector3 randomPos = new Vector3(Random.Range(-thisServer.mapWidth/2f,thisServer.mapWidth/2f),Random.Range(-thisServer.mapHeight/2f,thisServer.mapHeight/2f), -2);
			GameObject CellObj = PhotonNetwork.Instantiate("Cell", randomPos, Quaternion.identity, 0) as GameObject;
			CellObj.transform.parent = transform;
			CellObj.GetComponent<Cell>().myPlayer = this;
			photonView.RPC("addToParent", PhotonTargets.AllBuffered, CellObj.GetComponent<PhotonView>().viewID);
            FindLeader();
		}
	}
	[RPC] public void SetNickname(string nickname) {
		name = nickname;
	}
}
