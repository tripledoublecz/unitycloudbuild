using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Soomla;
using Soomla.Store;
using Soomla.Store.DotWars;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;

public class UIControl : MonoBehaviour {

	public GameObject gameMenu, gameOver, countDown, continueMenu, messageUI;
	float messageTimer;
	bool messageTimerStart = false;

	int countInt;

	public void StartGame() {
	}

	public void PlaySinglePlayer() {
		Application.LoadLevel ("SP_menu"); 
	}
	public void PlayMultiPlayer() {
		PlayerPrefs.SetInt("Online", 1);
		Application.LoadLevel("MP_menu");
	}
	public void OpenLeaderboard() {
		Social.ShowLeaderboardUI();
	}
	public void OpenStore() {
		Application.LoadLevel("Store");
	}
	public void Respawn() {
		gameOver.SetActive(false);
		StoreInventory.TakeItem(StoreInfo.Currencies[0].ItemId, 1);
		if (GameObject.Find("Countdown")) {
			GameObject.Find("Countdown").GetComponent<Countdown>().enabled = false;
			GameObject.Find("Countdown").SetActive(false);
		}

		GM.thisPlayer.SpawnCell();
		GM.thisPlayer.isGameOver = false;
	}

	public void OpenMainMenu() {
		PhotonNetwork.Disconnect();
		Application.LoadLevel("MainMenu");
	}
	public void OpenGameMenu() {
		gameMenu.SetActive(true);
    }
	public void OpenSettings() {
		Application.LoadLevel("Settings");
	}
    public void CloseGameMenu() {
		gameMenu.SetActive(false);
    }
	public void Split() {
		GM.thisPlayer.SplitCells();
	}
	public void Eject() {
		
	}
	public void StartSinglePlayerGame() {
		PlayerPrefs.SetInt("Online", 0);
		Application.LoadLevel("Game");
	}
	public void EnterNickname() {
		string nickname = GameObject.Find("Nickname_text").GetComponent<Text>().text;
		PlayerPrefs.SetString("Nickname", nickname);
	}

	public void OpenContinueMenu() {
		continueMenu.SetActive(true);
	}
	public void CloseContinueMenu() {
		continueMenu.SetActive(false);
	}

	public void PayForContinue() {
		continueMenu.SetActive(false);
		StoreInventory.TakeItem(StoreInfo.Currencies[0].ItemId, 2);
		gameOver.SetActive(false);
		
		if (GameObject.Find("Countdown")) {
			GameObject.Find("Countdown").GetComponent<Countdown>().enabled = false;
			GameObject.Find("Countdown").SetActive(false);
		}
		GM.thisPlayer.SpawnCell();
		GM.thisPlayer.isGameOver = false;
		GM.thisPlayer.leaderCell.mass = PlayerPrefs.GetInt("LastScore");
	}

	public void OpenElixirsStore() {
		Application.LoadLevelAdditive("Elixirs_store");
	}
	public void OpenGoldsStore() {
		Application.LoadLevelAdditive("Golds_store");
	}
	public void OpenMainStore() {
		Application.LoadLevelAdditive("Main_store");
	}

	public void CloseStore() {
		Destroy(GameObject.Find("Store_screen"));
		Destroy(gameObject);
	}

	public void Reconnect() {
		StoreInventory.GiveItem(StoreInfo.Currencies[0].ItemId, 1);
		PlayMultiPlayer();
	}

	public void ShowMessage(string message) {
		messageTimer = 4;
		messageTimerStart = true;
		messageUI.SetActive(true);
		messageUI.GetComponentInChildren<Text>().text = message;
	}

	public void RestorePurchase() {
		SoomlaStore.RestoreTransactions();
	}

	public void RateThisApp() {
		#if UNITY_ANDROID
		//Application.OpenURL("https://play.google.com/store/apps/details?id=com.blue88.dotwars");
        Application.OpenURL ("market://details?id=com.blue88.dotwars");
		#elif UNITY_IPHONE
		//Application.OpenURL("itms-apps://itunes.apple.com/app/1008746783");
        Application.OpenURL("itms-apps://itunes.apple.com/app/id1015934153");
		#endif
	}

	void Update() {
		if (messageTimerStart)
			messageTimer -= Time.deltaTime;

		if (messageTimerStart && messageTimer <= 0){
			messageTimerStart = false;
			messageUI.SetActive(false);
		}
	}
}
