﻿using UnityEngine;
using System.Collections;

public class Pattern : MonoBehaviour {

	void Awake () {
		if (PlayerPrefs.GetInt("DarkMode") == 1) {
			GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("Pattern_dark");
		} else {
			GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("Pattern");
		}
	}

	void Start () {
		GetComponent<Renderer>().material.mainTextureScale = new Vector2(256 , 256);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
