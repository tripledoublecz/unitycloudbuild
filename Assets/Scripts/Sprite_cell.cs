﻿using UnityEngine;
using System.Collections;

public class Sprite_cell : MonoBehaviour {

	void Start() {
		if (GetComponent<PhotonView>().isMine){
				if (PlayerPrefs.GetInt("Premium") == 0) {
					Debug.Log("Non Premium");
					ChangeSkinTo("");
				} else {
					//PhotonNetwork.Destroy(insideCell);
					ChangeSkinTo(PlayerPrefs.GetString("SkinSet"));
				}
		}
	}

	void FixedUpdate() {
		transform.rotation = Quaternion.identity;
	}

	[RPC] void ChangeSkinTo(string skinName) {
		GetComponent<SpriteRenderer>().color = Color.white;
		GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Avatars/" + skinName);
		
		if (GetComponent<PhotonView>().isMine)	
			GetComponent<PhotonView>().RPC("ChangeSkinTo", PhotonTargets.OthersBuffered, skinName);
	}
}
