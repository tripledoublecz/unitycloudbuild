﻿using UnityEngine;
using Soomla;
using Soomla.Store;
using System.Collections;
using System.Collections.Generic;

namespace Soomla.Store.DotWars {
	public class Store : IStoreAssets {

		public int GetVersion() {
			return 4;
		}

		public VirtualCurrency[] GetCurrencies() {
			return new VirtualCurrency[]{ELIXIR_CURRENCY, GOLD_CURRENCY};
		}

		public VirtualGood[] GetGoods() {
			return new VirtualGood[] {NO_ADS_NONCONS, RABBIT_SKIN, HOCKEY_SKIN, MONSTER_SKIN, CRITTER_SKIN, STORMTROOPER_SKIN, ALIEN_SKIN, HITLER_SKIN, FISH_SKIN, JOKER_SKIN};
		}

		public VirtualCurrencyPack[] GetCurrencyPacks() {
			return new VirtualCurrencyPack[] {FIFTYELIXIR_PACK, FIVEHUNDREDELIXIR_PACK, FIRSTGOLD_PACK, SECONDGOLD_PACK, THIRDGOLD_PACK};
		}

		public VirtualCategory[] GetCategories() {
			return new VirtualCategory[]{};
		}

		public const string ELIXIR_CURRENCY_ITEM_ID = "currency_elixir";
		public const string GOLD_CURRENCY_ITEM_ID = "currency_gold";


		public static VirtualCurrency ELIXIR_CURRENCY = new VirtualCurrency (
			"Elixirs",
			"",
			ELIXIR_CURRENCY_ITEM_ID
            );

		public static VirtualCurrency GOLD_CURRENCY = new VirtualCurrency (
			"Golds",
			"",
			GOLD_CURRENCY_ITEM_ID
		);
		public static VirtualCurrencyPack FIFTYELIXIR_PACK = new VirtualCurrencyPack(
			"50 Elixir",
			"Description",
			"elixir_50",
			50,
			ELIXIR_CURRENCY_ITEM_ID,
			new PurchaseWithMarket("buy_50_elixir", 0.99)
		);
		public static VirtualCurrencyPack FIVEHUNDREDELIXIR_PACK = new VirtualCurrencyPack(
			"500 Elixir",
			"Description",
			"elixir_500",
			500,
			ELIXIR_CURRENCY_ITEM_ID,
			new PurchaseWithMarket("buy_elixir_500", 1.99)
		);

		public static VirtualCurrencyPack FIRSTGOLD_PACK = new VirtualCurrencyPack(
			"500 Gold",
			"Description",
			"gold_500",
			500,
			GOLD_CURRENCY_ITEM_ID,
			new PurchaseWithMarket("buy_500_gold", 0.99)
			);
		public static VirtualCurrencyPack SECONDGOLD_PACK = new VirtualCurrencyPack(
			"1500 Gold",
			"Description",
			"gold_1500",
			1500,
			GOLD_CURRENCY_ITEM_ID,
			new PurchaseWithMarket("buy_1500_gold", 1.99)
			);
		public static VirtualCurrencyPack THIRDGOLD_PACK = new VirtualCurrencyPack(
			"10000 Gold",
			"Description",
			"gold_10000",
			10000,
			GOLD_CURRENCY_ITEM_ID,
			new PurchaseWithMarket("buy_10000_gold", 4.99)
			);

		public static VirtualGood NO_ADS_NONCONS = new LifetimeVG (
			"No Ads",
			"",
			"no_ads_item_id",
			new PurchaseWithVirtualItem(GOLD_CURRENCY_ITEM_ID, 100));
        
        /** Skiny **/
		public static VirtualGood RABBIT_SKIN = new LifetimeVG(
			"Rabbit Skin",
			"Skin",
			"rabbit_skin",
			new PurchaseWithVirtualItem(GOLD_CURRENCY_ITEM_ID, 500));
		public static VirtualGood HOCKEY_SKIN = new LifetimeVG(
			"Hockey Skin",
			"Skin",
			"hockey_skin",
			new PurchaseWithVirtualItem(GOLD_CURRENCY_ITEM_ID, 600));
		public static VirtualGood MONSTER_SKIN = new LifetimeVG(
			"Monster Skin",
			"Skin",
			"monster_skin",
			new PurchaseWithVirtualItem(GOLD_CURRENCY_ITEM_ID, 700));
        public static VirtualGood CRITTER_SKIN = new LifetimeVG(
			"Critter Skin",
			"Skin",
			"critter_skin",
			new PurchaseWithVirtualItem(GOLD_CURRENCY_ITEM_ID, 800));
		public static VirtualGood STORMTROOPER_SKIN = new LifetimeVG(
			"Stormtrooper Skin",
			"Skin",
			"stormtrooper_skin",
			new PurchaseWithVirtualItem(GOLD_CURRENCY_ITEM_ID, 900));
		public static VirtualGood ALIEN_SKIN = new LifetimeVG(
			"Alien Skin",
			"Skin",
			"alien_skin",
			new PurchaseWithVirtualItem(GOLD_CURRENCY_ITEM_ID, 1000));
		public static VirtualGood HITLER_SKIN = new LifetimeVG(
			"Hitler Skin",
			"Skin",
			"hitler_skin",
			new PurchaseWithVirtualItem(GOLD_CURRENCY_ITEM_ID, 2000));
		public static VirtualGood FISH_SKIN = new LifetimeVG(
			"Fish Skin",
			"Skin",
			"fish_skin",
			new PurchaseWithVirtualItem(GOLD_CURRENCY_ITEM_ID, 3000));
		public static VirtualGood JOKER_SKIN = new LifetimeVG(
			"Joker Skin",
			"Skin",
			"joker_skin",
			new PurchaseWithVirtualItem(GOLD_CURRENCY_ITEM_ID, 4000));
	}
}
