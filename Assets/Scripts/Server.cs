using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Server : Photon.MonoBehaviour {
	public static string typeName = "Mobile Game Project 5";
	private const string gameName = "World 4";
	
	private bool 	isRefreshingHostList = false,
					isServerRunning = false;
	public HostData[] hostList;
	
	public GameObject playerPrefab, playMenu, botPrefab, connectionLostPanel, virus, virusesParent;
	public static GameObject foodCell, foodCellsParent;
	public static Player thisPlayer;
	public int maxFoodCount;
	Button playButton;

	public GameObject splitGameButton;
	GameObject border;

	public string nickname;

	bool isOnline = false;
	int onlineInt;

	bool isServer = false;

	public int mapHeight = 512, 
	mapWidth = 512;
	int numberOfBots = 4;
	int numberOfViruses = 28;

	void Awake () {


		if (PlayerPrefs.GetInt("Online") == 1) {
			Debug.Log("Online");
		} else {
			PhotonNetwork.offlineMode = true; 
			PhotonNetwork.CreateRoom("OfflineRoom2");
			Debug.Log("Offline");
		}
    }
    
    void Start () {

		if (PhotonNetwork.offlineMode) {
			Debug.Log("Bug");
			for (int i = 1; i <= numberOfBots; i++) {
				//SpawnBot();
			}
		}
		foodCell = Resources.Load("food") as GameObject;
		foodCellsParent = GameObject.Find("FoodCells");
		border = Resources.Load("Border") as GameObject;
		maxFoodCount = 500;
		CreateBorders();
    }

	void FixedUpdate() {

		/*
		if ((foodCellsParent.transform.childCount < maxFoodCount) && PhotonNetwork.isMasterClient) {
			SpawnFood();
		}
		*/
		if ((virusesParent.transform.childCount < numberOfViruses) && PhotonNetwork.isMasterClient) {
			SpawnVirus();
		}
    }

	public void ButtonJoinServer() {
		RefreshHostList();
		if (hostList != null) {
			for (int i = 0; i < hostList.Length; i++)
			{
				JoinServer(hostList[0]);
            }
        }
    }
	void OnCreatedRoom() {
		isServerRunning = true;
		/*
		if (PhotonNetwork.offlineMode) {
			SpawnPlayer();
			splitGameButton.SetActive(true);
		}
		*/
	}
	
	private void RefreshHostList() {
		if (!isRefreshingHostList) {
			isRefreshingHostList = true;
			MasterServer.RequestHostList(typeName);
		}
	}
    
	private void JoinServer(HostData hostData) {
		//PhotonNetwork.JoinRoom(hostData);
	}
    
    void OnJoinedRoom() {
		Debug.Log("Joined");
		PhotonNetwork.Instantiate("PlayerPrefab", Vector3.up * 5, Quaternion.identity, 0);
		playMenu.SetActive(false);
		splitGameButton.SetActive(true);
	}

	void ShutdownServer() {
		foreach (Transform child in foodCellsParent.transform) {
			Destroy(child.gameObject);
		}
		PhotonNetwork.Disconnect();
		MasterServer.UnregisterHost();
    }
    
    void SpawnPlayer() {
		PhotonNetwork.Instantiate("PlayerPrefab", Vector3.up * 5, Quaternion.identity, 0);
		//Obj.GetComponent<Player>().name = nickname;
	}

	void SpawnBot() {
		Vector3 position = new Vector3(Random.Range(-mapWidth/2f,mapWidth/2f),Random.Range(-mapHeight/2f,mapHeight/2f), 1);
		GameObject Obj = Instantiate(botPrefab, position, Quaternion.identity) as GameObject;
		//Obj.GetComponent<Player>().name = nickname;
	}

	void SpawnFood() {
		Vector3 position = new Vector3(Random.Range(-mapWidth/2f,mapWidth/2f),Random.Range(-mapHeight/2f,mapHeight/2f), 4);
		GameObject Obj = PhotonNetwork.Instantiate("Food", position, Quaternion.identity, 0) as GameObject;
		Obj.transform.parent = foodCellsParent.transform;
		//photonView.RPC("addToParent", PhotonTargets.AllBuffered, foodCellsParent.GetComponent<PhotonView>().viewID);
	}

	void SpawnVirus() {
		int x = Random.Range((-mapWidth/4)/2,(mapWidth/4)/2);
		int y = Random.Range((-mapHeight/4)/2,(mapHeight/4)/2);
		Vector3 position = new Vector3(x*4,y*4, -2.54642f);
		GameObject Obj = PhotonNetwork.Instantiate("Virus", position, Quaternion.identity, 0) as GameObject;
		Obj.transform.parent = virusesParent.transform;
		//photonView.RPC("addToParent", PhotonTargets.AllBuffered, virusesParent.GetComponent<PhotonView>().viewID);
	}

	void CreateBorders() {
		GameObject borderObj;
		int gap = 4;
		//Top border
		borderObj = Instantiate(border, new Vector3(0,0,0), Quaternion.identity) as GameObject;
		Vector2[] points = borderObj.GetComponent<EdgeCollider2D>().points;
		points[0] = new Vector3(-mapWidth/2-gap, mapHeight/2+gap, 4);
		points[1] = new Vector3(mapWidth/2+gap, mapHeight/2+gap, 4);
		borderObj.GetComponent<EdgeCollider2D>().points = points;
		borderObj.GetComponent<LineRenderer>().SetPosition(0,points[0]);
		borderObj.GetComponent<LineRenderer>().SetPosition(1,points[1]);

		//Right border
		borderObj = Instantiate(border, new Vector3(0,0,0), Quaternion.identity) as GameObject;
		points[0] = new Vector3(mapWidth/2+gap, mapHeight/2+gap, 4);
		points[1] = new Vector3(mapWidth/2+gap, -mapHeight/2-gap, 4);
		borderObj.GetComponent<EdgeCollider2D>().points = points;
		borderObj.GetComponent<LineRenderer>().SetPosition(0,points[0]);
		borderObj.GetComponent<LineRenderer>().SetPosition(1,points[1]);

		//Bottom border
		borderObj = Instantiate(border, new Vector3(0,0,0), Quaternion.identity) as GameObject;
		points[0] = new Vector3(mapWidth/2+gap, -mapHeight/2-gap, 4);
		points[1] = new Vector3(-mapWidth/2-gap, -mapHeight/2-gap, 4);
		borderObj.GetComponent<EdgeCollider2D>().points = points;
		borderObj.GetComponent<LineRenderer>().SetPosition(0,points[0]);
		borderObj.GetComponent<LineRenderer>().SetPosition(1,points[1]);

		//Left border
		borderObj = Instantiate(border, new Vector3(0,0,0), Quaternion.identity) as GameObject;
		points[0] = new Vector3(-mapWidth/2-gap, -mapHeight/2-gap);
		points[1] = new Vector3(-mapWidth/2-gap, mapHeight/2+gap);
		borderObj.GetComponent<EdgeCollider2D>().points = points;
		borderObj.GetComponent<LineRenderer>().SetPosition(0,points[0]);
		borderObj.GetComponent<LineRenderer>().SetPosition(1,points[1]);
	}

	void OnPhotonPlayerDisconnected(PhotonPlayer player) {
		PhotonNetwork.RemoveRPCs(player);
		PhotonNetwork.DestroyPlayerObjects(player);
	}

	void OnDisconnectedFromServer(NetworkDisconnection info) {
		Debug.Log("Disconnected from server: " + info);
		Application.LoadLevel ("MP_menu");
	}
	public void OnDisconnectedFromPhoton()
	{
		connectionLostPanel.SetActive(true);
	}
	
	public void OnFailedToConnectToPhoton()
	{
		connectionLostPanel.SetActive(true);
	}
}
