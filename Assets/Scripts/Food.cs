using UnityEngine;
using System.Collections;

public class Food : Dot {

	PhotonView photonView;

	void Start () {
		gameObject.GetComponent<SpriteRenderer>().color = SetRandomColor();
		photonView = GetComponent<PhotonView>();
	}

	public bool eat = false;
	public GameObject eatedBy;
	public bool control = true;

	void FixedUpdate () {
		if (eat) {
			EatEffect();
		}
		if (eatedBy && control) {
			eatedBy.GetComponent<Cell>().mass++;
			control = false;
		}
	}

	void EatEffect() {
		transform.position = Vector2.Lerp(transform.position, eatedBy.transform.position, Time.deltaTime * 10);
		/*
		if (Vector2.Distance(transform.position, eatedBy.transform.position) < 0.4f) {
			if (PhotonNetwork.isMasterClient) {
				//eatedBy.GetComponent<Cell>().mass++;
				PhotonNetwork.RemoveRPCs(gameObject.GetComponent<PhotonView>());
				PhotonNetwork.Destroy(gameObject);
			}
		}
		*/
	}
}
