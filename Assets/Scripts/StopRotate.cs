﻿using UnityEngine;
using System.Collections;

public class StopRotate : MonoBehaviour {

	void FixedUpdate() {
		transform.rotation = Quaternion.identity;
	}
}
