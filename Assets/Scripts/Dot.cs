using UnityEngine;
using System.Collections;

public class Dot : MonoBehaviour {
	
	public float scale;
	public Color color;
	public int score;
	public string name;
	public float maxSpeed;

	void Start () {
	
	}

	public Color SetRandomColor() {
		Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
		return color;
	}

	public Vector3 SetRandomColorVector() {
		Vector3 color = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
		return color;
	}

	public Vector3 SetRandomColorVectorSwitch() {
		Vector3 color = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
		return color;
	}

	public void SetSize(float setSize) {
		Vector3 newSize = new Vector3(setSize,setSize,setSize);
		this.gameObject.transform.localScale = Vector3.Lerp (transform.localScale, newSize, 1f * Time.deltaTime);
	}
}
