﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;

public class GameCenter : MonoBehaviour {

	void Start () {
		Social.localUser.Authenticate ( success => { if (success) { Debug.Log("==iOS GC authenticate OK"); } else { Debug.Log("==iOS GC authenticate Failed"); } } );
		if (Social.localUser.authenticated) {
			Social.ReportScore(PlayerPrefs.GetInt("HighScore"), "DotWarLeaderboard", success => {
				Debug.Log(success ? "Reported score successfully" : "Failed to report score");
			});
		}
	}
}
