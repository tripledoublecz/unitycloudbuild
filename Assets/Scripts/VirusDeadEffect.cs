﻿using UnityEngine;
using System.Collections;

public class VirusDeadEffect : MonoBehaviour {

	float deadTime;
	void Start () {
		deadTime = 4;
	}
	
	// Update is called once per frame
	void Update () {
		deadTime -= Time.deltaTime;

		if (deadTime <= 0)
			PhotonNetwork.Destroy(this.gameObject);
	}
}
