﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Soomla;
using Soomla.Store;
using Soomla.Store.DotWars;
using System.Collections;

namespace Soomla.Store.DotWars {
	public class Elixirs : MonoBehaviour {

		public int elixirs;

		void Start () {
			if (!SoomlaStore.Initialized)
				SoomlaStore.Initialize(new Store());
	
			if (PlayerPrefs.GetInt("FirstStart") != 1) {
				PlayerPrefs.DeleteAll();
				StoreInventory.GiveItem(StoreInfo.Currencies[0].ItemId, 10);
				StoreInventory.GiveItem(StoreInfo.Currencies[1].ItemId, 0);
				PlayerPrefs.SetInt("FirstStart", 1);
				PlayerPrefs.SetInt("Joystick", 1);
				PlayerPrefs.SetInt("HighScore", 0);
			}
			//PlayerPrefs.SetInt("Premium", 1);
		}

		void Update() {
			if (GameObject.Find("Elixirs_count")) {
				//GameObject.Find("Elixirs_count").GetComponent<Text>().text = "" + PlayerPrefs.GetInt("Elixirs");
				GameObject.Find("Elixirs_count").GetComponent<Text>().text = "" + StoreInventory.GetItemBalance(StoreInfo.Currencies[0].ItemId);
			}
			if (GameObject.Find("Golds_count")) {
				GameObject.Find("Golds_count").GetComponent<Text>().text = "" + StoreInventory.GetItemBalance(StoreInfo.Currencies[1].ItemId);
			}

			if (GameObject.Find("Elixirs_count_menu")) {
				//GameObject.Find("Elixirs_count").GetComponent<Text>().text = "" + PlayerPrefs.GetInt("Elixirs");
				GameObject.Find("Elixirs_count_menu").GetComponent<Text>().text = "" + StoreInventory.GetItemBalance(StoreInfo.Currencies[0].ItemId);
			}
			if (GameObject.Find("Golds_count_menu")) {
				GameObject.Find("Golds_count_menu").GetComponent<Text>().text = "" + StoreInventory.GetItemBalance(StoreInfo.Currencies[1].ItemId);
            }
			if (Input.GetKeyDown(KeyCode.Escape)) 
				Application.Quit();
        }

		public void Buy50Elixirs() {
			StoreInventory.BuyItem(Store.FIFTYELIXIR_PACK.ItemId);
		}
		public void Buy500Elixirs() {
			StoreInventory.BuyItem(Store.FIVEHUNDREDELIXIR_PACK.ItemId);
		}
		public void Buy500Gold() {
			StoreInventory.BuyItem(Store.FIRSTGOLD_PACK.ItemId);
		}
		public void Buy1500Gold() {
			StoreInventory.BuyItem(Store.SECONDGOLD_PACK.ItemId);
		}
		public void Buy10000Gold() {
			StoreInventory.BuyItem(Store.THIRDGOLD_PACK.ItemId);
		}
	}
}
