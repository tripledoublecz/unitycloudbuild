﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using System.Collections;
using Soomla;
using Soomla.Store;
using Soomla.Store.DotWars;

public class GameOver : MonoBehaviour {

	bool timeStarted = false;
	float timer = 0;
	string minutes;
	string seconds;
	Text countDownText;
	Button continueButton;

	void Awake() {
		if (Advertisement.isSupported) {
			Advertisement.allowPrecache = true;
			#if UNITY_ANDROID
			Advertisement.Initialize ("47542", true);
			#endif
			#if UNITY_IPHONE
			Advertisement.Initialize ("47537", true);
			#endif
		} else {
			Debug.Log("Platform not supported");
		}
	}
	
	void OnEnable() {

        if (StoreInventory.GetItemBalance(StoreInfo.Goods[0].ItemId) == 0) {
			Advertisement.Show(null, new ShowOptions {
				pause = true,
				resultCallback = result => {
					Debug.Log(result.ToString());
				}
			});
		}
		
		
		if (GameObject.Find("Elixirs_row")) {
			int count;
			if (StoreInventory.GetItemBalance(StoreInfo.Currencies[0].ItemId) < 10) {
				count = StoreInventory.GetItemBalance(StoreInfo.Currencies[0].ItemId);
			} else {
				count = 10;
			}
			Transform elixirsRow = GameObject.Find("Elixirs_row").transform;
			
			foreach(Transform child in elixirsRow) {
				Destroy(child.gameObject);
			}
			
			for (int i = 1; i <= count; i++) {
				GameObject iconObj = Instantiate(Resources.Load("UI/Elixir_icon")) as GameObject;
				iconObj.transform.SetParent(elixirsRow,false);
			}
			if (count < 10) {
				for (int i = 1; i <= 10-count; i++) {
					GameObject iconObj = Instantiate(Resources.Load("UI/Elixir_empty_icon")) as GameObject;
					iconObj.transform.SetParent(elixirsRow,false);
				}
			}
		}
	}
}
