﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Leaderboard : MonoBehaviour {
    
	//SortedDictionary<int, string> highscores = new SortedDictionary<int, string>();
	int[] scoreInt = new int[10];
	string[] namesString = new string[10];
	GameObject[] allPlayers;
	int myScore;
	Text scorePos;

	void Start () {
		InvokeRepeating("UpdateLeaderboard", 0, 2);
		scorePos = GameObject.Find("ScorePos").GetComponent<Text>();
	}

	void UpdateLeaderboard() {
		allPlayers = GameObject.FindGameObjectsWithTag("Player");
		for (int i=0; i < PhotonNetwork.playerList.Length; i++) {
			scoreInt[i] = allPlayers[i].GetComponent<Player>().score;
			namesString[i] = allPlayers[i].GetComponent<Player>().name;
		}
        
		SortScores(scoreInt, namesString);
	}

	void SortScores (int[] scores, string[] names)
	{
		int tmp;
		int tmp2;
		String tmpName;
		int tmpid;
		
		for (int i = 0; i < scores.Length - 1; i++)
		{
			tmp = i;
			for (int j = i + 1; j < scores.Length; j++) {
				if (scores[j] > scores[tmp])
					tmp = j;
			}

			tmp2 = scores[tmp];
			tmpName = names[tmp];

			scores[tmp] = scores[i];
			scores[i] = tmp2;
			names[tmp] = names[i];
            names[i] = tmpName;
			if (GM.thisPlayer.score == scores[i]) {
				int pos = tmp+1;
				if (pos == 1)
					scorePos.text = pos + "st of " + allPlayers.Length + " players";
				if (pos == 2)
					scorePos.text = pos + "nd of " + allPlayers.Length + " players";
				if (pos == 3) 
					scorePos.text = pos + "rd of " + allPlayers.Length + " players";
				if (pos > 3) 
					scorePos.text = pos + "th of " + allPlayers.Length + " players";
			}
        }
		int length = Mathf.Clamp(allPlayers.Length, 1, 10);
		foreach (Transform child in transform) {
			Destroy(child.gameObject);
        }
		/*
        for (int i = 0; i < length; i++) {
			GameObject player = Instantiate(Resources.Load<GameObject>("UI/Leaderboard_player"));
			player.transform.SetParent(transform, false);
			player.GetComponent<Text>().text = "" + names[i] + ": " + scores[i];
        }
        */
    }

}
