using UnityEngine;
using System.Collections;

public class CameraClass : MonoBehaviour {

	public static Player thisPlayer;

	void Start () {
	
	}

	void Update() {
		if (thisPlayer)
			Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, 0.6f+thisPlayer.leaderCellObj.transform.localScale.x*10, 0.2f);
	}
	
	void FixedUpdate () {
		if (thisPlayer && thisPlayer.leaderCellObj) {
			if (thisPlayer.transform.childCount > 1 ) {
				Vector2[] childrens = new Vector2[thisPlayer.transform.childCount];
				int i = 0;
				foreach (Transform child in thisPlayer.transform) {
					childrens[i] = child.position;
					i++;
				}
				transform.position = new Vector3(CenterOfVectors(childrens).x,CenterOfVectors(childrens).y, -10f);
			} else {
				transform.position = new Vector3(thisPlayer.leaderCell.transform.position.x,thisPlayer.leaderCell.transform.position.y, -10f);
			}
		}
	}

	public Vector2 CenterOfVectors(Vector2[] vectors)
	{
		Vector2 sum = Vector2.zero;
		if( vectors == null || vectors.Length == 0 )
		{
			return sum;
		}
		
		foreach( Vector2 vec in vectors )
		{
			sum += vec;
		}
        return sum/vectors.Length;
    }
}
