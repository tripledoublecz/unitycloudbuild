﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using System.Collections;
using Soomla;
using Soomla.Store;
using Soomla.Store.DotWars;

public class Countdown : MonoBehaviour {

	bool timeStarted = false;
	float timer = 0;
	int timerInt;
	string minutes;
	string seconds;
	Text countDownText;
	Button respawnButton, continueButton;
	public GameObject playButton, flaskTimer;
	bool isActive;
	bool initialized = false;

	void Start() {

		countDownText = GameObject.Find("Countdown_text").GetComponent<Text>();

		if (StoreInventory.GetItemBalance(StoreInfo.Currencies[0].ItemId) > 0) {
			timeStarted = false;
			PlayerPrefs.SetInt("Timer", 0);
			playButton.SetActive(true);
			flaskTimer.SetActive(false);
		} else {
			if (PlayerPrefs.GetInt("Timer") <= 0) {
				PlayerPrefs.SetInt("Timer", 120);
			}
			timeStarted = true;
			playButton.SetActive(false);
			flaskTimer.SetActive(true);
		}
		timer = PlayerPrefs.GetInt("Timer");
	}

	void Update () {
		minutes = Mathf.Floor(timer / 60).ToString("00");
		seconds = (timer % 60).ToString("00");

		if (StoreInventory.GetItemBalance(StoreInfo.Currencies[0].ItemId) > 0) {
			timeStarted = false;
			PlayerPrefs.SetInt("Timer", 0);
			playButton.SetActive(true);
			flaskTimer.SetActive(false);
		}
		if (timeStarted) {
			if (timer > 0) {
				timer -= Time.deltaTime;
				timerInt = (int) timer;
				PlayerPrefs.SetInt("Timer", timerInt);
				playButton.SetActive(false);
			} else {
				StoreInventory.GiveItem(StoreInfo.Currencies[0].ItemId,5);
				timeStarted = false;
				Debug.Log("Přidán elixír");
				GameObject.Find("UI").GetComponent<UIControl>().ShowMessage("5 Elixirs added");
			}
		}
		countDownText.text = minutes + ":" + seconds;
	}
}
