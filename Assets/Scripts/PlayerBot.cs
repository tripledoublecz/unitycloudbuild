using UnityEngine;
using System.Collections;

public class PlayerBot : Dot {
	
	public Rigidbody2D rigidBody;
	
	public int mass = 2;
	bool followLeader = true;
	float endTime;
	bool followAfterShot = false;
	bool foundFood = false;
	GameObject FoodCells,
	target;
	bool goingSomewhere = false;

	Server thisServer;
	
	GameObject arrow;
	Vector3 targetPos;
	
	void Start () {
		thisServer = GameObject.Find("GM").GetComponent<Server>();
		FoodCells = GameObject.Find("FoodCells");
		rigidBody = gameObject.GetComponent<Rigidbody2D>();
		ChangeColor();
		
		foreach (Transform child in transform) {
			if (child.tag == "Arrow") {
				arrow = child.gameObject;
				break;
			}
        }
	}
	
	void FixedUpdate () {

		if (GM.thisPlayer) { //Exception
			if (GM.thisPlayer.leaderCellObj && Vector2.Distance(GM.thisPlayer.leaderCellObj.transform.position, transform.position) < 4) {
				ChasingPlayer();
			} else {
				if (!goingSomewhere) {
						if (GM.thisPlayer.leaderCellObj && Vector2.Distance(GM.thisPlayer.leaderCellObj.transform.position, transform.position) < 4) {
							goingSomewhere = true;
							ChasingPlayer();
						} else {
							int randomInt = Random.Range(1,3);
							switch(randomInt) {
								case 1:FindFood();break;
								case 2:GoSomewhere();break;
							}
						}
					} else {
						GoHere(targetPos);
					}
			}
		}

		Vector3 vectorToTarget = targetPos - transform.position;
		float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
		Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
		transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 2);

		scale = (mass * 0.004f) + 0.2f;
		maxSpeed = 6 - mass * 0.01f;
		this.gameObject.transform.localScale = Vector3.Lerp (transform.localScale, new Vector3(scale,scale,scale), 4f * Time.deltaTime);
	}

	void FindFood() {
		goingSomewhere = true;
		float closestDistance = 10000;
		GameObject closestFood = null;

		foreach(Transform child in FoodCells.transform) {
			if (Vector2.Distance(transform.position, child.position) < closestDistance) {
				closestDistance = Vector3.Distance(transform.position, child.position);
				closestFood = child.gameObject;
			}
		}
		targetPos = closestFood.transform.position;
	}

	void GoSomewhere() {
		goingSomewhere = true;
		targetPos = new Vector3(Random.Range(-thisServer.mapWidth/2f,thisServer.mapWidth/2f),Random.Range(-thisServer.mapHeight/2f,thisServer.mapHeight/2f), 1);
	}

	void GoHere(Vector3 goHere) {
		transform.position = Vector3.MoveTowards(transform.position, goHere, 0.04f);
		if (Vector3.Distance(goHere, transform.position) < 0.1f) {
			goingSomewhere = false;
		}
	}

	void ChasingPlayer() {
		targetPos = GM.thisPlayer.leaderCellObj.transform.position;
		transform.position = Vector3.MoveTowards(transform.position, GM.thisPlayer.leaderCellObj.transform.position, 0.04f);
	}

	void OnTriggerStay2D(Collider2D otherCore){
		if (otherCore.tag == "Food") {
			if (otherCore.GetComponent<Food>()) {
				otherCore.GetComponent<Food>().eatedBy = gameObject;
				otherCore.GetComponent<Food>().eat = true;
				mass++;
			}
		}
		
		if (otherCore.tag == "Player") {
			if (otherCore.GetComponent<Player>().score < score) {
				score += otherCore.GetComponent<Player>().score;
			}
		}
	}
	
	void OnTriggerEnter2D(Collider2D otherCore) {
		if (otherCore.tag == "Core" && mass > otherCore.transform.parent.GetComponent<Cell>().mass) {
			mass += otherCore.transform.parent.GetComponent<Cell>().mass;
            PhotonNetwork.Destroy(otherCore.transform.parent.gameObject);
        }
    }
    
    void ChangeColor() {
        ChangeColorTo(new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)));
    }
    
    void ChangeColorTo(Vector3 color) {
        GetComponent<SpriteRenderer>().color = new Color(color.x, color.y, color.z, 1f);
	}
}
