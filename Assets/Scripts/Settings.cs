using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Soomla;
using Soomla.Store;
using Soomla.Store.DotWars;

public class Settings : MonoBehaviour {

	public bool setJoystick, setDarkMode;
	Toggle joystickToggle, darkModeToggle;
	GameObject skinCollection, skinSelected;

	void Awake() {
		joystickToggle = GameObject.Find("Toggle_joystick").GetComponent<Toggle>();
		darkModeToggle = GameObject.Find("Toggle_dark_mode").GetComponent<Toggle>();
		
		if (PlayerPrefs.GetInt("Joystick") == 1) {
			joystickToggle.isOn = true;
		} else {
			joystickToggle.isOn = false;
		}
		
		if (PlayerPrefs.GetInt("DarkMode") == 1) {
			darkModeToggle.isOn = true;
		} else {
			darkModeToggle.isOn = false;
		}
	}

	void Start () {
		GameObject.Find("Nickname_input").GetComponent<InputField>().text = PlayerPrefs.GetString("Nickname");

		/** Skins **/
		SoomlaStore.Initialize(new Store());
		skinCollection = GameObject.Find("Skin_collection");
		skinSelected = GameObject.Find("Skin_selected");

		skinSelected.GetComponent<Image>().sprite = Resources.Load<Sprite>("Avatars/" + PlayerPrefs.GetString("SkinSet"));
		Debug.Log(PlayerPrefs.GetString("SkinSet"));
		
		foreach(VirtualGood vg in StoreInfo.Goods){
			if (vg.Name != "No Ads") {
				if (StoreInventory.GetItemBalance(vg.ItemId) > 0) {
					GameObject skinObject = Instantiate(Resources.Load("Skin_settings")) as GameObject;
					skinObject.transform.SetParent(skinCollection.transform, false);
					skinObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("Avatars/" + vg.Name);
					string captured = vg.Name;
					skinObject.GetComponent<Button>().onClick.AddListener(() => { SetSkin(captured);});
	            }
			}
        }
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log(PlayerPrefs.GetInt("Premium"));
        
        if (joystickToggle.isOn) {
			PlayerPrefs.SetInt("Joystick",1);
		} else {
			PlayerPrefs.SetInt("Joystick",0);
		}

		if (darkModeToggle.isOn) {
			PlayerPrefs.SetInt("DarkMode",1);
		} else {
			PlayerPrefs.SetInt("DarkMode",0);
		}
	}

	public void SetSkin(string skin) {
		skinSelected.GetComponent<Image>().sprite = Resources.Load<Sprite>("Avatars/" + skin);
		PlayerPrefs.SetString("SkinSet", skin);
	}
}
