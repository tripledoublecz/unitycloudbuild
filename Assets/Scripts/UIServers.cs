using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIServers : Photon.MonoBehaviour {

	public HostData[] hostList;
	bool isRefreshingHostList = false,
	isServerRunning = false;
	float timer;
	public GameObject refreshButton;
	int roomID;

	public GameObject serverList,
		serverRow;

	void Awake() {
	}

	void Start() {
		PhotonNetwork.ConnectUsingSettings("v4.2");
		if (PhotonNetwork.connectedAndReady) {
			PhotonNetwork.JoinLobby ();
		}
		//RefreshServerList();
		timer = 20;
		roomID = 1;
    }
    
    void Update() {
        
        /*
		if (isRefreshingHostList && MasterServer.PollHostList().Length > 0)
		{
			isRefreshingHostList = false;
			hostList = MasterServer.PollHostList();
			Debug.Log(hostList.Length);

			if (hostList != null) {
				for (int i = 0; i < hostList.Length; i++)
				{
					AddServerRow(hostList[i].gameName, i);
				}
			}
		}
		*/
		timer -= Time.deltaTime;
		if (timer <= 0) {
			GameObject.Find("Connecting_text").GetComponent<Text>().text = "Internet connection failed";
			refreshButton.SetActive(true);
		}
	}
	void OnConnectedToMaster() {
		Debug.Log("Connected to master");
	}

	void OnJoinedLobby() {
		Debug.Log("Joined lobby");
		RoomOptions newRoomOptions = new RoomOptions();
		newRoomOptions.isOpen = true;
		newRoomOptions.isVisible = true;
		newRoomOptions.maxPlayers = 10;
		// in this example, C0 might be 0 or 1 for the two (fictional) game modes
		newRoomOptions.customRoomProperties = new ExitGames.Client.Photon.Hashtable() { { "C0", 1 } };
		newRoomOptions.customRoomPropertiesForLobby = new string[] { "C0" }; // this makes "C0" available in the lobby
		
		// let's create this room in SqlLobby "myLobby" explicitly
		TypedLobby sqlLobby = new TypedLobby("myLobby", LobbyType.SqlLobby);

		PhotonNetwork.JoinOrCreateRoom("DotWars" + roomID, newRoomOptions, TypedLobby.Default);

		PhotonNetwork.LoadLevel("Game");

		Debug.Log("Created room");
    }
	void OnPhotonJoinRoomFailed () {
		roomID++;
	}
}
